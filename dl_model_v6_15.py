from dl_model_v6 import *


version = 'v6_15'
SEED = 73
random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
dtype = torch.float64
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
BATCH_SIZE = 128

N_EPOCHS = 100
CLIP = 1
USECOLS = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'nwp_lat', 'nwp_lon', 'pwp_lat', 'pwp_lon']
PREDICT_COLS = ['lat', 'lon', 'alt']  # east north up in this case
INPUT_DIM = len(USECOLS)
IN_CHANNELS = len(PREDICT_COLS)  # len(USECOLS)
OUT_CHANNELS = 16
DEC_INPUT_DIM = 16
DEC_OUTPUT_DIM = 3
KERNEL_SIZE = 3
INPUT_SEQ_LEN = 15
OUTPUT_SEQ_LEN = 45  # 60
LATENT_DIM = 32
ALPHA = 0.25

# default `log_dir` is "runs"
writer = SummaryWriter(f'runs/{version}/')
path = Path(f'model/{version}')
if not path.exists():
    path.mkdir()
path = Path(f'images/{version}')
if not path.exists():
    path.mkdir()

class Status:
    train_running_loss = 0.0
    train_replacement_loss = 0.0
    train_velo_loss = 0.0
    train_iter = 0

    valid_running_loss = 0.0
    valid_replacement_loss = 0.0
    valid_velo_loss = 0.0
    valid_iter = 0

# model.load_state_dict(torch.load(f'model/{version}/CONVO-GRU-model-server2.220.pt'))
# model.load_state_dict(torch.load(f'model/v5/CONVO-GRU-model-server1.720.pt'))


if __name__ == '__main__':
    print(torch.__version__)
    print(device)
    main()
