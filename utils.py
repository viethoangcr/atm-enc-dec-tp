import numpy as np
import pyproj
from pyproj import Transformer, Geod
from typing import List, Tuple, Optional, Union
import logging
from scipy.interpolate import interp1d
import pandas as pd
from haversine import haversine_vector, haversine, Unit
from shapely.geometry import Point, LineString
from shapely.ops import nearest_points
from sklearn.metrics.pairwise import paired_cosine_distances

import pymap3d as pm

from sklearn.neighbors import KDTree

GisLocation = Tuple[float, float]  # lat, lon
R_Earth = 6366830  # meter, at Paris location

ecef = pyproj.Proj(proj='geocent', ellps='WGS84', datum='WGS84')
lla = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
lla2ecef_tranformer = Transformer.from_crs(lla.crs, ecef.crs)
ecef2lla_tranformer = Transformer.from_crs(ecef.crs, lla.crs)
geod = Geod(ellps="WGS84")


def nm2ft(x):
    return x * 6076.1155


def ft2nm(x):
    return x / 6076.1155


def lla2enu(lat, lon, h, lat0, lon0, h0=0, **kwargs):
    """

    Args:
        lat:
        lon:
        h: in ft
        lat0:
        lon0:
        h0: in ft
        **kwargs:

    Returns: values nm

    """
    ft2m = 0.3048
    nm2m = 1852.
    h, h0 = h * ft2m, h0 * ft2m
    e, n, u = pm.geodetic2enu(lat, lon, h, lat0, lon0, h0, **kwargs)
    return e / nm2m, n / nm2m, u / nm2m
    # return e, n, u


def enu2lla(e, n, u, lat0, lon0, h0, **kwargs):
    """

    Args:
        e: in nautical miles (nm)
        n: in nm
        u: in nm
        lat0: in degree if deg=True (default)
        lon0: in degree if deg=True (default)
        h0: in feet
        **kwargs:

    Returns:
        lat: in degree if deg=True (default)
        lon: in degree if deg=True (default)
        h: in ft
    """
    ft2m = 0.3048
    nm2m = 1852.
    e, n, u = e * nm2m, n * nm2m, u * nm2m
    lat, lon, h = pm.enu2geodetic(e, n, u, lat0, lon0, h0 * ft2m, **kwargs)
    return lat, lon, h / ft2m


def lla2ecef(lat, lon, alt, radians=False):
    x, y, z = lla2ecef_tranformer.transform(lon, lat, alt, radians=radians)
    return x, y, z


def ecef2lla(x, y, z, radians=False):
    lon, lat, alt = ecef2lla_tranformer.transform(x, y, z, radians=radians)
    return lat, lon, alt


def curvature(points: np.ndarray) -> np.ndarray:
    dp_dt = np.gradient(points, axis=0)
    ds2_dt = np.sum(dp_dt * dp_dt, axis=1)
    d2p_dt2 = np.gradient(dp_dt, axis=0)
    curvature_ = np.abs(d2p_dt2[:, 0] * dp_dt[:, 1] - dp_dt[:, 0] * d2p_dt2[:, 1]) / ds2_dt**1.5
    return curvature_


def curvature_latlon(points: np.ndarray, step=5, window=2):
    """

    Args:
        points: numpy array of (n_points, 2)
        step:
        window:

    Returns:

    """
    data_np = points
    n_row = data_np.shape[0]
    curv_ = []
    for pos in range(step, n_row, step):
        start = step if pos == step else step + window
        end = window if pos + step < n_row else n_row - pos
        arr = data_np[pos - start:pos + end]

        lat0, lon0, _ = arr[len(arr) // 2, :3]
        lat, lon, alt = arr[:, 0], arr[:, 1], arr[:, 2]

        e, n, _ = lla2enu(lat, lon, alt, lat0, lon0, 0)
        c = curvature(np.array([e, n]).T)

        curv_.extend(list(c[(0 if start == step else window):(-window if pos + step < n_row else None)]))

    assert len(curv_) == n_row
    return curv_


class MultiInterpolation:
    def __init__(self, x: List[float], *args: List[List[float]], inter_func=None, **kwargs):
        n = len(x)
        self._models = []
        self.inter_func = inter_func if inter_func is not None else interp1d
        for y in args:
            if len(y) != n:
                raise ValueError("Parameters does not have the same number of elements")
            self._models.append(self.inter_func(x, y, **kwargs))
        self.x_range = (x[0], x[-1])
        self._kwargs = kwargs

    def add_dimensions(self, *args):
        if len(args) % 2 != 0:
            raise ValueError("Number of lists passed should be even")

        it = iter(args)
        for x, y in zip(it, it):
            self._models.append(self.inter_func(x, y, **self._kwargs))

    def __call__(self, x: List[float], *args, **kwargs):
        return tuple([f(x) for f in self._models])


class LatLonInterpolation:
    """ Refer to scipy interpolation function (interp) for usage"""
    def __init__(self, timestamps: List[float], locations: List[GisLocation], velocity_smoothing=False) -> None:
        if len(timestamps) != len(locations):
            raise ValueError("2 arrays must have the same number of location")
        self._timestamps, self._locations = zip(*sorted(zip(timestamps, locations), key=lambda x: x[0]))

        self._distances, self._azimuths = [], []
        for i in range(len(self._locations) - 1):
            az12, _, dist = geod.inv(
                self._locations[i][1], self._locations[i][0],  # lon, lat
                self._locations[i+1][1], self._locations[i+1][0],  # lon, lat
            )
            self._distances.append(dist)
            self._azimuths.append(az12)

        # smoothing velocity
        self.velocity_smoothing = velocity_smoothing
        if velocity_smoothing:
            time_diff = np.subtract(self._timestamps[1:], self._timestamps[:-1])
            self.velocities = np.zeros_like(time_diff)
            self.velocities = self._distances / time_diff

            self.acceleration = np.zeros_like(self.velocities)
            self.acceleration[:-1] = (self.velocities[1:] - self.velocities[:-1]) / time_diff[:-1]

    def __call__(
            self,
            timestamps: Union[List[float], Tuple[float], 'np.ndarray', float],
            raise_error=True, *args, **kwargs
    ) -> Union[List[Optional[GisLocation]], GisLocation]:
        is_list = isinstance(timestamps, (np.ndarray, tuple, list))
        idx = np.searchsorted(self._timestamps, timestamps, side='right')

        id_max = len(self._timestamps)

        def get_loc(id, timestamp) -> Optional[GisLocation]:
            if id <= 0 or id > id_max:
                if raise_error:
                    raise ValueError(f"{timestamp} is out of range ({self._timestamps[0]}, {self._timestamps[-1]})")
                else:
                    return np.nan, np.nan
            elif id == id_max:
                return self._locations[-1]
            id -= 1
            be, af = self._timestamps[id], self._timestamps[id+1]
            if self.velocity_smoothing:
                del_t = timestamp - be
                dist = self.velocities[id] * del_t + 0.5 * del_t * del_t * self.acceleration[id]
                pass
            else:
                dist = (timestamp - be) / (af - be) * self._distances[id]
            lon, lat, _ = geod.fwd(self._locations[id][1], self._locations[id][0], self._azimuths[id], dist)
            return lat, lon

        if is_list:
            return [get_loc(i, ts) for i, ts in zip(idx, timestamps)]
        elif isinstance(timestamps, (int, float)):
            return get_loc(idx, timestamps)
        else:
            raise ValueError("Incorrect type of timestamp")


def mapping_waypoints2(
        trajectory: pd.DataFrame,
        waypoints: pd.DataFrame,
        max_distance=7.,  # 2 nautical miles
        max_distance_straight=2.,
        min_waypoint_distance=50,  # 50 nautical miles
        traj_column_name=("longitude", "latitude", "posix_time"),
        wp_column_name=("LONGITUDE", "LATITUDE", "IDENT"),
        curvature_threshold=1.,
        ignore_loop=True,
        create_virtual_waypoint=True,
) -> pd.DataFrame:
    """

    Args:
        trajectory:
        waypoints:
        max_distance:
        min_waypoint_distance:
        traj_column_name:
        wp_column_name:

    Returns:

    """
    tr_lon, tr_lat, tr_time = traj_column_name
    wp_lon, wp_lat, wp_name = wp_column_name

    # wp_e, wp_n, _ = lla2enu(waypoints[wp_lat].values, waypoints[wp_lon].values)

    tr_total_distance = haversine_vector(
        trajectory[[tr_lat, tr_lon]].values[1:, :],
        trajectory[[tr_lat, tr_lon]].values[:-1, :],
        unit=Unit.NAUTICAL_MILES
    )


    pass



def mapping_waypoints(
        trajectory: pd.DataFrame,
        waypoints: pd.DataFrame,
        max_distance=7.,  # 2 nautical miles
        max_distance_straight=2.,
        min_waypoint_distance=50,  # 50 nautical miles
        traj_column_name=("longitude", "latitude", "posix_time"),
        wp_column_name=("LONGITUDE", "LATITUDE", "IDENT"),
        curvature_threshold=1.,
        ignore_loop=True,
        create_virtual_waypoint=True,
) -> pd.DataFrame:
    """This function will NOT give correct result for trajectory pass the 180° line of longitude"""
    # if type(waypoints) == pd.DataFrame:
    #     if 'latitude' not in waypoints.columns or 'longitude' not in waypoints.columns:
    #         raise ValueError(f"Could not found column latitude or longitude in dataframe")
    #     waypoints = KDTree()
    tr_lon, tr_lat, tr_time = traj_column_name
    wp_lon, wp_lat, wp_name = wp_column_name
    kdtree = KDTree(trajectory[[tr_lon, tr_lat]])

    line = LineString(list(zip(trajectory[tr_lon].values, trajectory[tr_lat].values)))
    if ignore_loop and (not line.is_simple):
        return []
    points = list(map(lambda x: Point(x[0], x[1]), waypoints[[wp_lon, wp_lat]].itertuples(index=False, name=None)))
    points_at_nn = [nearest_points(line, p) for p in points]
    arr1 = np.array([(p.y, p.x) for p, _ in points_at_nn])
    arr2 = np.array([(p.y, p.x) for _, p in points_at_nn])
    distances_nm = haversine_vector(arr1, arr2, unit=Unit.NAUTICAL_MILES)
    # distances_nm = list(map(
    #     lambda p1, p2: haversine((p1.y, p1.x), (p2.y, p2.x), unit=Unit.NAUTICAL_MILES),
    #     points_at_nn
    # ))
    res = []
    f = lambda p: (f_lat(p.y) + f_lon(p.x)) / 2.
    for id, dis in enumerate(distances_nm):
        if dis < max_distance:
            res.append({
                'distance': dis,
                'p_lat': points_at_nn[id][0].y, 'p_lon': points_at_nn[id][0].x,
                'wp_lat': points_at_nn[id][1].y, 'wp_lon': points_at_nn[id][1].x,
                'ident': waypoints[wp_name].iloc[id],
                # 'posix_time': float(f(points_at_nn[id][0]))
            })
    # query closest point to get time passing waypoint
    if len(res) < 1: return []
    res_df = pd.DataFrame(res)
    _, closet_points = kdtree.query(res_df[['p_lon', 'p_lat']].values, k=2)
    res_temp = []
    for cps, p, r in zip(closet_points, res_df[['p_lon', 'p_lat']].values, res):
        tdf = trajectory[['lat', 'lon', 'timestamp']].iloc[cps].values
        f_lat = interp1d(tdf[:, 0], tdf[:, 2], fill_value='extrapolate')
        f_lon = interp1d(tdf[:, 1], tdf[:, 2], fill_value='extrapolate')
        time_lat = f_lat(p[1]).item()
        time_lon = f_lon(p[0]).item()
        if abs(time_lat - time_lon) > 30:
            logging.error(f"ERROR in computing posix time: {abs(time_lat - time_lon)}")
            continue
        r['posix_time'] = (time_lat + time_lon) / 2.
        r['tr_idx'] = min(cps)
        res_temp.append(r)
    res = res_temp

    # sort by time and filter closed waypoints
    if len(res) > 1:
        res.sort(key=lambda x: x['tr_idx'])
        res_filtered = []

        is_curved = trajectory['curvature'] > curvature_threshold
        curved_group = (is_curved != is_curved.shift(1)).cumsum().values
        group_candidates = {}
        group_check_points = {}

        for wp in res:
            if (not is_curved.iloc[wp['tr_idx']]) and (wp['distance'] < max_distance_straight):
                res_filtered.append(wp)
            elif is_curved.iloc[wp['tr_idx']] or \
                    ((wp['tr_idx'] + 2) < len(is_curved) and is_curved.iloc[wp['tr_idx'] + 2]):
                tr_idx = wp['tr_idx'] if is_curved.iloc[wp['tr_idx']] else wp['tr_idx'] + 2
                current_group = curved_group[tr_idx]

                if current_group not in group_check_points:
                    # find list of max 5 points, very heuristics
                    max_point = 5
                    point_list = []

                    for i in range(tr_idx, -1, -1):
                        if curved_group[i] == current_group:
                            continue
                        elif curved_group[i] == current_group - 1:
                            point_list.append(i)
                            if len(point_list) == max_point:
                                break
                        else:
                            break
                    group_check_points[current_group] = point_list
                else:
                    point_list = group_check_points[current_group]

                # compute the angle these point and the waypoint
                if len(point_list) < 2:
                    score = dis
                else:
                    points = trajectory[[tr_lon, tr_lat]].iloc[point_list].values
                    vec_a = points[:-1] - points[1:]
                    vec_b = np.array([wp['wp_lon'], wp['wp_lat']]) - points[:-1]
                    score = np.mean(paired_cosine_distances(vec_a, vec_b))

                if current_group not in group_candidates or score < group_candidates[current_group][1]:
                    group_candidates[current_group] = (wp, score)

        # Generate waypoint for curved group
        def compute_virtual_wp(pre_group: pd.DataFrame, cur_group: pd.DataFrame):
            pre_group = pre_group[pre_group['curvature'] < 0.5 * curvature_threshold]
            if pre_group.shape[0] < 2:
                return None
            else:
                p1, p2 = pre_group[[tr_lat, tr_lon, tr_time]].values[-2:, :]
                a = p2 - p1
                i = cur_group['curvature'].argmax()
                t = cur_group[tr_time].iloc[i]
                wp = p2[:2] + a[:2] * ((t - p2[2]) / a[2])
                eps = np.random.randn(2)
                lat, lon, _ = enu2lla(eps[0], eps[1], 0, wp[0], wp[1], 0)
                return lat, lon

        if create_virtual_waypoint:
            gps = trajectory.groupby(curved_group)
            for group_id, group_track in gps:
                if (group_id > 1) and (group_id < len(gps)) and \
                        (group_id not in group_candidates) and group_track['curvature'].max() > 2.*curvature_threshold:
                    pre_group = gps.get_group(group_id - 1)
                    wp = compute_virtual_wp(pre_group, group_track)
                    i = group_track['curvature'].argmax()
                    p = group_track.iloc[i]
                    if wp is not None:
                        group_candidates[group_id] = ({
                            'distance': -1,
                            'p_lat': p[tr_lat], 'p_lon': p[tr_lon],
                            'wp_lat': wp[0], 'wp_lon': wp[1],
                            'ident': f'VIR_WP_{group_id}',
                            'tr_idx': group_track.index[i],
                            'posix_time': p[tr_time]
                        }, None)

        res_filtered += [wp for wp, _ in group_candidates.values()]
        res_filtered.sort(key=lambda x: x['tr_idx'])
        # filter by minimum distance (for non-curved parts only)
        res = res_filtered

        if len(res) < 1: return []

        res_filtered = [res[0]]
        for point in res[1:]:
            pre_point = res_filtered[-1]
            d = haversine(
                (pre_point['wp_lat'], pre_point['wp_lon']),
                (point['wp_lat'], point['wp_lon']), unit=Unit.NAUTICAL_MILES)
            if d > min_waypoint_distance:
                res_filtered.append(point)
            elif is_curved.iloc[point['tr_idx']]:
                res_filtered[-1] = point
            else:
                if (not is_curved.iloc[pre_point['tr_idx']]) and (pre_point['distance'] > point['distance']):
                    res_filtered[-1] = point

        return res_filtered
    else:
        return res


# def compute_error(loc_pred, loc_true):
#     pass


if __name__ == "__main__":
    import pandas as pd
    from haversine import haversine_vector, Unit

    import matplotlib.pyplot as plt


    df = pd.read_csv('./test_data/test_track.csv')
    ts = df['timestamp'].values
    latlon = df[['lat', 'long']].values

    plt.scatter(latlon[:, 1], latlon[:, 0])

    def rmse(pred_loc, true_loc):
        error_distance = haversine_vector(pred_loc, true_loc, unit=Unit.METERS)
        error_distance2 = np.sort(error_distance)

        rmse_func = lambda d: np.sqrt(np.mean(np.power(d, 2)))

        res = rmse_func(error_distance)
        res_90 = rmse_func(error_distance2[:int(0.9 * len(error_distance2))])
        return res, res_90

    #
    gaps = 30
    idx = list(np.arange(ts.shape[0])[::gaps])
    if idx[-1] != (len(ts) - 1): idx.append(len(ts) - 1)

    f_lat, f_lon = interp1d(ts[idx], latlon[idx][:, 0], kind="cubic"),\
                   interp1d(ts[idx], latlon[idx][:, 1], kind="cubic")
    pred_latlon = f_lat(ts), f_lon(ts)
    # plt.plot(pred_latlon[1], pred_latlon[0])
    print(rmse(np.vstack(pred_latlon).T, latlon))

    f_inter = LatLonInterpolation(ts[idx], latlon[idx])
    pred_latlon = np.array(f_inter(ts))
    # plt.plot(pred_latlon[1], pred_latlon[0])
    print(rmse(pred_latlon, latlon))

    f_inter = LatLonInterpolation(ts[idx], latlon[idx], velocity_smoothing=True)
    pred_latlon = np.array(f_inter(ts))
    plt.scatter(pred_latlon[1], pred_latlon[0])
    print(rmse(pred_latlon, latlon))

    plt.show()

    pass