from path import Path

output_path = Path(r'output')
eff_folder = Path(r'/home/viethoangcr/Documents/Dataset/SIA_flight_plans/EFF')
eff_folder = Path(r'/media/viethoangcr/Data/SIADataset/EFF')


dataset_folder = Path(r'/home/viethoangcr/Documents/Dataset/Dataset')
FLIGHT_DATA_PATH = dataset_folder / 'ADSB_2019_6MONTH_FLIGHTS.csv.gz'
TRACK_DATA_PATH = dataset_folder / 'ADSB_2019_6MONTH_TRACKS.csv.gz'
