import requests
import logging
import pandas as pd
import numpy as np
import re

from path import Path
from typing import Optional, List
from pygeodesy.ellipsoidalKarney import LatLon

logger = logging.getLogger(__name__)

COUNTRIES = pd.read_csv('./additional_data/countries.csv', sep='\t')
BASE_URL = r"https://opennav.com/waypoint/"
SAVED_DATA_LOCATION = Path("./additional_data/")
WAYPOINT_FILENAME = "waypoints_{country_code}.csv"


def _format_table(df: pd.DataFrame) -> pd.DataFrame:
    def format_str(s: str):
        s = s.strip()
        if ("'" not in s) and ('°' not in s):
            return f'{s[:-5]}° {s[-5:-3]}\' {s[-3:-1]}" {s[-1]}'
        elif '-' in s:
            p = s.split('-')
            return f'{p[0]}° {p[1]}\' {p[2][:-1]}" {p[2][-1]}'
        else:
            return s

    res = df.dropna(axis=0, how="all")
    res.dropna(axis=1, how="all", inplace=True)
    lats, longs = [], []
    for lat, lon in zip(res["LATITUDE"].values, res["LONGITUDE"].values):
        try:
            ll = LatLon(format_str(lat), format_str(lon))
            lats.append(ll.lat)
            longs.append(ll.lon)
        except Exception as e:
            logger.exception(e)
            lats.append(np.nan)
            longs.append(np.nan)

    res["LATITUDE"] = lats
    res["LONGITUDE"] = longs
    return res


def _get_country(countries: Optional[List[str]] = None):
    if countries is None:
        countries = COUNTRIES
    else:
        countries = [s.lower() for s in countries]
        idx = (COUNTRIES["Alpha-2 code"].str.lower().isin(countries)) | \
              (COUNTRIES["Alpha-3 code"].str.lower().isin(countries)) | \
              (COUNTRIES["Country"].str.lower().isin(countries))
        countries: pd.DataFrame = COUNTRIES[idx]
    return countries


def update_waypoints(countries: Optional[List[str]] = None):
    countries = _get_country(countries)

    for code, name in countries[["Alpha-2 code", "Country"]].itertuples(index=False, name=None):
        logger.info(f"Updating waypoint list of {name}")
        try:
            dfs = pd.read_html(BASE_URL + code, header=0, match='LONGITUDE')
            if dfs and dfs[0].shape[0] > 1:
                df = _format_table(dfs[0])
                df = df.dropna(how='all')
                df.to_csv(SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code), index=False)
                logger.info(f"Updated successfully with {df.shape[0]} waypoints for {name}")
                if df['LATITUDE'].isna().sum() > 0:
                    logger.warning(f'{name} having {df["LATITUDE"].isna().sum()} empty values')
            else:
                logger.warning(f"Could not find waypoint table for {name}")
        except ValueError as ve:
            logger.info(f"No waypoint tables found for {name}")
        except Exception as e:
            logger.error(f"Error while updating for {name}")
            logger.exception(e)


def get_waypoints(countries: Optional[List[str]] = None, update=None) -> pd.DataFrame:
    countries = _get_country(countries)

    if update is True or update is None:
        need_to_update_list = []
        for code in countries["Alpha-2 code"].values:
            filepath = SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code)
            if not filepath.exists():
                need_to_update_list.append(code)

        update_waypoints(need_to_update_list)

    dfs = []
    for code, name in countries[["Alpha-2 code", "Country"]].itertuples(index=False, name=None):
        filepath = SAVED_DATA_LOCATION / WAYPOINT_FILENAME.format(country_code=code)
        if filepath.exists():
            df = pd.read_csv(filepath)
            df['Country'] = code
            df['Country Fullname'] = name
            dfs.append(df)

    return pd.concat(dfs, ignore_index=True)


def get_sg_significant_point(url: str) -> pd.DataFrame:
    def convert_decimal(coord: str):
        lat, lon = re.sub('([NSEW])', '\\1 ', coord).split()

        if lat[-1] == 'N':
            lat_direction = 1
        else:
            lat_direction = -1

        f = lambda x: x // 10000 + ((x % 100) / 60 + (x // 100 % 100)) / 60

        decimal_lat = f(float(lat[:-1]))
        decimal_lat = lat_direction * decimal_lat

        if lon[-1] == 'E':
            lon_direction = 1
        else:
            lon_direction = -1
        decimal_lon = f(float(lon[:-1]))
        decimal_lon = lon_direction * decimal_lon

        return decimal_lat, decimal_lon
    dfs = pd.read_html(url)
    df: pd.DataFrame = dfs[0]
    df.columns = ['WaypointName', 'Coordinates', 'ATSRoute', 'TerminalArea']
    df['ATSRoute'] = df['ATSRoute'].str.replace('[^\x00-\x8F]', '')

    latlon = df['Coordinates'].apply(convert_decimal)
    latlon_df = pd.DataFrame(latlon.tolist(), columns=['Latitude', 'Longitude'], index=latlon.index)
    return pd.concat([df, latlon_df], axis=1)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    update_waypoints()
    get_sg_significant_point(r'https://aim-sg.caas.gov.sg/aip/2020-10-13/final/2020-09-10-Non-AIRAC/html/eAIP/ENR-4.4-en-GB.html')
