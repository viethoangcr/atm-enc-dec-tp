import pandas as pd
import numpy as np
import os
from functools import lru_cache
from copy import deepcopy
from path import Path

from typing import Union
from functools import reduce

import torch
from torch.utils.data import Dataset
from utils import lla2enu, enu2lla, curvature

from config import FLIGHT_DATA_PATH, TRACK_DATA_PATH


def get_flight_data(
        sampling_rate: float = None, seed: int = None, flight_filter={}, track_filter={}
) -> Union[pd.DataFrame, pd.DataFrame]:
    """[summary]

    Keyword Arguments:
        sampling_rate {float} -- [description] (default: {None})
        seed {int} -- [description] (default: {None})

    Raises:
        ValueError: [description]

    Returns:
        pd.DataFrame -- [description]
    """

    def filter_df(df: pd.DataFrame, conditions: dict = None):
        if conditions:
            idx = reduce(lambda x, y: x & y, [func(df[col_name]) if callable(func) else df[col_name] == func for col_name, func in flight_filter.items()])
            return df[idx]
        else:
            return df

    if sampling_rate is not None and (sampling_rate > 1. or sampling_rate <= 0.):
        raise ValueError(f"sampling_rate={sampling_rate} is NOT in range (0, 1)")
    else:
        np.random.seed(seed)
        # read only 1 column for sampling data
        cols = ['flight_id'] + list(flight_filter.keys())
        flight: pd.DataFrame = pd.read_csv(FLIGHT_DATA_PATH, compression='gzip', usecols=cols)
        flight_idx = filter_df(flight, flight_filter)
        # flight_idx = flight[]
        selected_index = np.random.choice(
            flight_idx.index, int(sampling_rate * flight_idx.shape[0]),
            replace=False) if sampling_rate and sampling_rate < 1. else flight_idx.index.values
        nrows = np.max(selected_index) + 1
        ignore_idx = np.setdiff1d(flight.index.values, selected_index)
        ignore_idx = np.sort(ignore_idx + 1)

    # idx + 1 to ignore the header row
    flight_data: pd.DataFrame = pd.read_csv(FLIGHT_DATA_PATH, compression='gzip', skiprows=ignore_idx, nrows=nrows)

    # read track data
    selected_flight_idx = flight_data['flight_id']

    flight_idx: pd.DataFrame = pd.read_csv(TRACK_DATA_PATH, compression='gzip', usecols=['flight_id'])
    selected_row_idx = flight_idx[flight_idx.isin(set(selected_flight_idx)).values].index.values

    nrows = np.max(selected_row_idx) + 1
    ignore_idx = np.setdiff1d(flight_idx.index.values, selected_row_idx) + 1
    track_data = pd.read_csv(TRACK_DATA_PATH, compression='gzip', skiprows=ignore_idx, nrows=nrows)

    if track_filter:
        track_data = filter_df(track_data, track_filter)

    return flight_data, track_data


class TrackDataset(Dataset):
    def __init__(
            self, track_data: pd.DataFrame, n_input, n_output, transpose=False,
            step=1, curvature_values=None, curvature_threshold=1./50,  # 50 nautical mile radius
            *args, **kwargs
    ):
        self._data = track_data.values
        self._n_input = n_input
        self._n_output = n_output
        self._size = (track_data.shape[0] - (n_input + n_output) + 1) // step
        self._transpose_func = (lambda x: x.T) if transpose else (lambda x: x)
        self._step = step
        self.meta = {k:(v.values if type(v) == pd.Series else v) for k, v in kwargs.items()}

        self.curvature_values = curvature_values
        if curvature_values is not None:
            self.curvature_threshold = curvature_threshold
            self.is_curved = curvature_values > curvature_threshold
            self.curved_group = (self.is_curved != self.is_curved.shift(1)).cumsum().values
            self.max_idx = curvature_values.groupby(self.curved_group).idxmax()

        assert self._size > 0

    def convert_to_neu(self, inp, lat0, lon0, h0=0):
        n = inp.shape[0]
        default_h = np.full(n, h0)
        e, n, u = lla2enu(inp[:, 0], inp[:, 1], inp[:, 2], lat0, lon0, h0)
        inp[:, 0], inp[:, 1], inp[:, 2] = e, n, u

        e, n, _ = lla2enu(inp[:, 3], inp[:, 4], default_h, lat0, lon0, h0)
        inp[:, 3], inp[:, 4] = e, n

        e, n, _ = lla2enu(inp[:, 5], inp[:, 6], default_h, lat0, lon0, h0)
        inp[:, 5], inp[:, 6] = e, n

        e, n, _ = lla2enu(inp[:, 7], inp[:, 8], default_h, lat0, lon0, h0)
        inp[:, 7], inp[:, 8] = e, n

    def __getitem__(self, item):
        item *= self._step
        pos = item + self._n_input
        inp = deepcopy(self._data[item:pos])
        cur_nwp = inp[-1, [5, 6]]
        lat0, lon0, _ = inp[-1, :3]
        self.convert_to_neu(inp, lat0, lon0, 0)

        outp = deepcopy(self._data[pos:(pos + self._n_output)])
        temp = np.array((np.isclose(outp[:, 7], cur_nwp[0]), np.isclose(outp[:, 8], cur_nwp[1]))).all(axis=0)
        temp[-1] = True
        n_seq = temp.argmax() + 1
        self.convert_to_neu(outp, lat0, lon0, 0)

        if self.curvature_values is not None:
            coup = self.curved_group[pos:(pos + self._n_output)]
            if coup.min() == coup.max() and not self.is_curved[pos]:
                phase = 1  # 'Straight'
            else:
                if coup[-1] - coup[0] > 1 and not self.is_curved[pos]:
                    phase = 4  # 'All Curve'
                elif self.is_curved[pos] and pos < self.max_idx[coup[0]]:
                    phase = 2  # 'Entering Turn'
                elif self.is_curved[pos] and pos >= self.max_idx[coup[0]]:
                    phase = 3  # 'Exiting Turn'
                else:
                    phase = 0
        else:
            phase = 0
        meta = {k:v[pos-1] for k, v in self.meta.items()}
        meta['n_seq'] = n_seq
        meta['phase'] = phase
        meta['origin'] = (lat0, lon0)
        return \
            torch.from_numpy(self._transpose_func(inp)), \
            torch.from_numpy(self._transpose_func(outp)), \
            n_seq, phase, (lat0, lon0), meta
            

    def __len__(self):
        return self._size


@lru_cache(maxsize=16)
def _read_csv(filepath: str, **kwargs) -> pd.DataFrame:
    df = pd.read_csv(filepath, **kwargs)
    df['row_idx'] = df["flight_id"] + "_" + df.index.astype(str)
    return df


class TrajectoryDataset():
    def __init__(
            self, input_file, usecols=None,
            flight_id='flight_id', path_format="{date}_track_data_10.csv.gz"
    ):
        df = pd.read_csv(input_file)
        self.flight_data = list(df.itertuples(name='Row'))
        self._flight_id_dict = {r.flight_id: r.Index for r in self.flight_data}
        self._data_folder = Path(os.path.dirname(input_file))
        self._path_format = path_format
        self.usecols = usecols
        self._flight_id_str = flight_id

    def __getitem__(self, index):
        if isinstance(index, int):
            item = self.flight_data[index]
        elif isinstance(index, str):
            i = self._flight_id_dict[index]
            item = self.flight_data[i]
        else:
            raise TypeError()
        file_path = self._data_folder / self._path_format.format(date=item.date)
        df = _read_csv(file_path)
        df = df[df[self._flight_id_str] == item.flight_id]
        return df[self.usecols] if self.usecols else df

    def __len__(self):
        return len(self.flight_data)
