import os
import logging
import json
import math
from itertools import groupby

import pandas as pd
import numpy as np
from scipy.interpolate import CubicSpline
from haversine import haversine_vector, Unit
import matplotlib.pyplot as plt

from config import output_path, dataset_folder
from dataset import get_flight_data
from utils import MultiInterpolation, curvature

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

TIME_STEP = 10
MANEUVERS_PERIOD = math.ceil(20 * 60 / TIME_STEP) + 1  # check every 20 minutes


def generate_data():
    files = os.listdir(output_path)

    flight_df = pd.read_csv(dataset_folder / 'ADSB_2019_6MONTH_FLIGHTS.csv.gz')

    timings = [pd.read_csv(output_path / file) for file in files if file.endswith('_timings.csv')]
    waypoints = [pd.read_csv(output_path / file) for file in files if file.endswith('_waypoints.csv')]

    timing_df = pd.concat(timings, ignore_index=True)
    waypoint_df = pd.concat(waypoints, ignore_index=True)

    flight_df = flight_df[~flight_df['STD'].isna()]
    flight_std =pd.to_datetime(flight_df['STD'], unit='s')

    timing_std = pd.to_datetime(timing_df.STD)

    res = pd.merge(
        flight_df, timing_df, how='left',
        left_on=[flight_df['callsign'].str.lower(), flight_std],
        right_on=[timing_df['callsign'].str.lower(), timing_std]
    )

    res1 = res[~res['callsign_y'].isna()]
    res1.to_csv('test.csv', index=False)

    flight_id_mapping = res1[['flight_id_x', 'flight_id_y']].drop_duplicates('flight_id_x', keep='first')
    id_mapping = {
        y: x for x, y in flight_id_mapping.itertuples(index=False, name=None)
    }

    gps = waypoint_df[['waypointname', 'latitude', 'longitude']].groupby(
        waypoint_df['flight_id'], group_keys=False
    )

    wps_dict = {
        id_mapping[fid]: df.to_dict('records') for fid, df in gps if fid in id_mapping
    }

    # export to json
    with open('data/flight_plan.json', 'wt') as fp:
        json.dump(wps_dict, fp)

    flight_idx = res1["flight_id_x"].values
    flight_data, track_data = get_flight_data(None, flight_filter={
        'flight_id': lambda x: x.isin(flight_idx)
    })

    flight_data.to_csv('data/flight_data.csv.gz', index=False)
    track_data.to_csv('data/track_data.csv.gz', index=False)


def re_sampling_data(time_step=TIME_STEP):
    with open('data/flight_plan.json', 'rt') as fp:
        wps_dict = json.load(fp)

    track_data = pd.read_csv('data/track_data.csv.gz')
    flight_data = pd.read_csv('data/flight_data.csv.gz')

    gps = track_data.groupby('flight_id')
    res_dict = {}
    fids = []
    for fid, df in gps:
        df = df[df['calibrated_altitude_ft'] > 2000]
        df = df.sort_values('UTC')
        df = df.drop_duplicates(['lat', 'lon'], keep='last')

        t = df[['lat', 'lon']]
        dis = haversine_vector(t.values[:-1, :], t.values[1:, :], unit=Unit.METERS)
        time_diff = df['UTC'].diff().values[1:]
        speed = dis / time_diff
        if np.any(speed > 500) or np.any(speed < 20):
            logger.info(f"Ignore flight {fid} with min speed {speed.min()} and max_speed {speed.max()} | {len(res_dict)}")
            continue

        if (df['date'].unique().size > 1) or (df.shape[0] < 100) or (fid not in wps_dict):
            continue
        utc = df['UTC'].values
        lat, lon, alt = df['lat'].values, df['lon'].values, df['calibrated_altitude_ft'].values
        f = MultiInterpolation(utc, lat, lon, alt, inter_func=CubicSpline)
        nts = np.arange((utc[0] // time_step + 1) * time_step, utc[-1], time_step)
        nlat, nlon, nalt = f(nts)
        curvature_ = curvature(np.array((nlat, nlon)).T)
        new_df = pd.DataFrame({
            'flight_id': fid,
            'timestamp': nts,
            'lat': nlat,
            'lon': nlon,
            'alt': nalt,
            'curvature': curvature_,
        })
        wps = wps_dict[fid]
        wp_df = pd.DataFrame(wps)

        # add waypoint information to data
        dis = haversine_vector(
            wp_df[['latitude', 'longitude']].values, new_df[['lat', 'lon']].values,
            unit=Unit.NAUTICAL_MILES, comb=True
        )
        wp_df['index'] = dis.argmin(axis=0)
        wp_df = wp_df[dis.min(axis=0) < 20]  # only select the one with less than 20 nautical miles
        if wp_df.shape[0] < 5: continue
        wp_df = wp_df.set_index('index')  # current heading waypoint
        next_wp_df = wp_df.shift(-1)

        wp_df['phase'] = wp_df['waypointname'].isin(['T_O_C', 'T_O_D']).cumsum()

        wp_df.rename({
            'waypointname': 'wp_name', 'latitude': 'wp_lat', 'longitude': 'wp_lon'
        }, axis=1, inplace=True)
        next_wp_df.rename({
            'waypointname': 'nwp_name', 'latitude': 'nwp_lat', 'longitude': 'nwp_lon'
        }, axis=1, inplace=True)
        wp_df = wp_df.join(next_wp_df, how='left')

        new_df = new_df.join(wp_df, how='left')
        new_df = new_df.bfill().ffill()

        res_dict[fid] = new_df
        fids.append(fid)

    logger.info(f"Finished processing {len(res_dict)} flights")

    flight_data = flight_data[flight_data['flight_id'].isin(fids)]
    flight_data.to_csv(f'data/train_data{TIME_STEP}/flight_data.csv')

    fgps = flight_data.groupby('date')

    for date, df in fgps:
        logger.info(f"Saving data for date {date}")
        res = [res_dict[fid] for fid in df['flight_id'].values]
        logger.info(f"Date {date} has {len(res)} flights")
        new_track_data = pd.concat(res, ignore_index=True)
        new_track_data.to_csv(
            f'data/train_data{TIME_STEP}/{date}_track_data_{time_step}.csv.gz', index=False, float_format="%.9f"
        )


def split_train_test():
    df = pd.read_csv(f'data/train_data{TIME_STEP}/flight_data.csv')
    train_df = df[df['date'] <= '2019-03-25']
    test_df = df[(df['date'] > '2019-03-25') & (df['date'] < '2019-03-28')]
    train_df.to_csv(f'data/train_data{TIME_STEP}/flight_data_small_train.csv', index=False)
    test_df.to_csv(f'data/train_data{TIME_STEP}/flight_data_small_test.csv', index=False)


if __name__ == "__main__":
    # re_sampling_data()
    split_train_test()
