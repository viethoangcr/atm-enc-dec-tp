import matplotlib.pyplot as plt
import numpy as np


def plot_traj(input, true_output, pred_output, wp=None, figsize=(15, 15)):
    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(input[:, 0], input[:, 1], 'b.')
    ax.plot(true_output[:, 0], true_output[:, 1], 'g.')
    ax.plot(pred_output[:, 0], pred_output[:, 1], 'r.')
    if wp is not None:
        ax.plot(wp[:, 0], wp[:, 1], 'k^')
    plt.axis('square')
    return fig


