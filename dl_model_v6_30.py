from abc import ABC
import numpy as np
import pandas as pd
import random
import os
import time

from functools import lru_cache
from copy import deepcopy
from path import Path

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
import torch.nn.modules.loss as L
import torch.nn.functional as F

import matplotlib.pyplot as plt

from torch.utils.tensorboard import SummaryWriter

from utils import lla2enu, enu2lla
from visualization import plot_traj


version = 'v6_30'
SEED = 73
random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
dtype = torch.float64
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
BATCH_SIZE = 128

N_EPOCHS = 100
CLIP = 1
USECOLS = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'nwp_lat', 'nwp_lon', 'pwp_lat', 'pwp_lon']
PREDICT_COLS = ['lat', 'lon', 'alt']  # east north up in this case
INPUT_DIM = len(USECOLS)
IN_CHANNELS = len(PREDICT_COLS)  # len(USECOLS)
OUT_CHANNELS = 16
DEC_INPUT_DIM = 16
DEC_OUTPUT_DIM = 3
KERNEL_SIZE = 3
INPUT_SEQ_LEN = 10
OUTPUT_SEQ_LEN = 45  # 60
LATENT_DIM = 32
ALPHA = 0.25

# default `log_dir` is "runs"
writer = SummaryWriter(f'runs/{version}/')
path = Path(f'model/{version}')
if not path.exists():
    path.mkdir()
path = Path(f'images/{version}')
if not path.exists():
    path.mkdir()


class Encoder(nn.Module):
    def __init__(self, input_dim, output_dim, in_channels, out_channels, kernel_size, **kwargs):
        super().__init__(**kwargs)
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.kernel_size = kernel_size

        self.feature = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(out_channels * (input_dim - kernel_size + 1),
                      output_dim),
            nn.ReLU()
        )

    def forward(self, input):
        # input = [batch, in_channels, input_dim]
        x = self.feature(input)
        return x


class Decoder(nn.Module):
    def __init__(self, input_dim, output_dim, hid_dim, **kwargs):
        super().__init__(**kwargs)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hid_dim = hid_dim

        self.gru = nn.GRU(input_dim + hid_dim, hid_dim, batch_first=True)

        self.fc_out = nn.Linear(input_dim + hid_dim * 2, output_dim)

    def forward(self, input: torch.Tensor, context: torch.Tensor, hidden: torch.Tensor):
        # input = [batch, input_dim]
        # context = [batch, 1, hid_dim]
        # hidden = [1, batch, hid_dim]

        input1 = input.unsqueeze(1)
        x = torch.cat((input1, context), dim=2)
        # x = [batch, 1, input_dim + hid_dim]

        output, hidden = self.gru(x, hidden)
        # output = [batch, 1, hid_dim]
        # hidden = [1, batch, hid_dim]
        output_ = torch.cat((input, output.squeeze(1), hidden.squeeze(0)), dim=1)
        pred = self.fc_out(output_)
        # pred = [batch, output_dim]
        return pred, hidden


class TpDeepModel(nn.Module, ABC):
    def __init__(self, encoder, decoder, device, dtype, **kwargs):
        super().__init__(**kwargs)

        self.encoder = encoder
        self.decoder = decoder
        self.device = device
        self.dtype = dtype

        assert encoder.output_dim == decoder.hid_dim, \
            "Hidden dimensions of encoder and decoder must be equal!"

    def forward(self, inputs, outputs, waypoints, teacher_forcing_ratio=0.5):
        """

        Args:
            inputs: the input sequence
            outputs: training mode if outputs is a Tensor, predicting mode if outputs is
                an integer. In this case, the output sequence will have length of outputs
            teacher_forcing_ratio: the probability for training mode

        Returns:

        """

        # inputs = [batch, input_n_feature, seq_len]
        # outputs = [batch, output_seq_len, out_n_feature]
        # teacher_forcing_ratio
        assert isinstance(outputs, (torch.Tensor, int)), "Not a valid type for outputs"
        batch_size = inputs.shape[0]
        mode = 0 if isinstance(outputs, torch.Tensor) else 1  # 0: training, 1: predict
        output_seq_len = outputs.shape[1] if mode == 0 else outputs
        result = torch.zeros(batch_size, output_seq_len, self.decoder.output_dim).to(
            device=self.device, dtype=self.dtype)

        velo_result = torch.zeros(batch_size, output_seq_len+1, self.decoder.output_dim).to(
            device=self.device, dtype=self.dtype)

        context = self.encoder(inputs[:, :3, :])
        hidden = context.unsqueeze(0)
        context = context.unsqueeze(1)

        # input = torch.zeros(batch_size, self.decoder.output_dim).to(
        #     device=self.device, dtype=self.dtype)
        cur_pos = inputs[:, :3, -1]
        prev_pos = inputs[:, :3, -2]
        cur_wp = inputs[:, 3:5, -1]
        next_wp = inputs[:, 5:7, -1]
        prev_wp = inputs[:, 7:9, -1]

        velo_result[:, 0, :] = cur_pos - prev_pos

        for i in range(0, output_seq_len):
            # cur_wp = waypoints[:, i, 0:2]
            # next_wp = waypoints[:, i, 2:4]
            # prev_wp = waypoints[:, i, 4:6]

            inp = self.compute_feature(cur_pos, prev_pos, prev_wp, cur_wp, next_wp)
            # [batch, 16]

            output, hidden = self.decoder(inp, context, hidden)

            # outputs[:, i, :] = x0 + output
            result[:, i, :] = output
            velo_result[:, i+1, :] = output - cur_pos  # velocity loss

            prev_pos = cur_pos

            teacher_force = random.random() < teacher_forcing_ratio
            if mode == 0 and teacher_force:
                cur_pos = outputs[:, i, :]
            else:
                cur_pos = output

            # passed_wp = self.check_pass_wp(cur_pos, prev_pos, cur_pos)


        return result, velo_result

    cos = nn.CosineSimilarity(dim=1, eps=1e-6)

    def compute_feature(
            self, c_pos: torch.Tensor, pre_pos: torch.Tensor,
            pwp: torch.Tensor, cwp: torch.Tensor, nwp: torch.Tensor):
        # c_pos = pre_pos = [batch, 3]  (e, n, u)
        # pwp = cwp = nwp = [batch, 2]  (lat, lon) (e, n)
        batch_size = c_pos.shape[0]
        zeros = torch.zeros(batch_size, 1, device=c_pos.device, dtype=c_pos.dtype)
        vel = c_pos - pre_pos
        dis_wp_vec = cwp - c_pos[:, :2]
        dist_wp = dis_wp_vec.norm(dim=1).view(batch_size, -1)
        cur_cos = self.cos(vel[:, :2], dis_wp_vec).view(batch_size, -1)

        c_pos2 = c_pos[:, :2]
        cross_distance1 = (torch.norm(
            torch.cross(torch.cat((cwp-pwp, zeros), dim=1), torch.cat((pwp-c_pos2, zeros), dim=1), dim=1),
            dim=1) / torch.norm(cwp-pwp, dim=1)).view(batch_size, -1)

        dis_nwp_vec = nwp - c_pos2
        dist_nwp = dis_nwp_vec.norm(dim=1).view(batch_size, -1)
        next_cos = self.cos(vel[:, :2], dis_nwp_vec).view(batch_size, -1)
        cross_distance2 = (torch.norm(
            torch.cross(torch.cat((nwp-cwp, zeros), dim=1), torch.cat((cwp-c_pos2, zeros), dim=1), dim=1),
            dim=1) / torch.norm(nwp-cwp, dim=1)).view(batch_size, -1)

        res = torch.cat((
            c_pos, vel,  # 3 + 3 = 6
            dis_wp_vec, dist_wp, cur_cos, cross_distance1,  # 2 + 1 + 1 + 1 = 5
            dis_nwp_vec, dist_nwp, next_cos, cross_distance2  # 2 + 1 + 1 + 1 = 5
        ), dim=1)  # return shape (batch, 16)
        res[res != res] = 0
        return res

    def check_pass_wp(self, c_pos: torch.Tensor, pwp: torch.Tensor, cwp: torch.Tensor):
        # c_pos = pre_pos = [batch, 3]  (e, n, u)
        # pwp = cwp = [batch, 2]  (lat, lon) or (e, n)
        f = lambda v1, v2: (v1 * v2).sum(axis=1, keepdim=True)
        # get E, N only
        c_pos = c_pos[:, :2]
        v1 = c_pos - pwp
        v2 = cwp - pwp
        t = f(v1, v2)
        return t > f(v2, v2)  # [batch, 1]


class TrackDataset(Dataset):
    def __init__(
            self, track_data: pd.DataFrame, n_input, n_output, transpose=False,
            step=1, device=device, dtype=dtype
    ):
        self._data = track_data.values
        self._n_input = n_input
        self._n_output = n_output
        self._size = (track_data.shape[0] - (n_input + n_output) + 1) // step
        self._transpose_func = (lambda x: x.T) if transpose else (lambda x: x)
        self._dev = device
        self._dt = dtype
        self._step = step
        assert self._size > 0

    def convert_to_neu(self, inp, lat0, lon0, h0=0):
        n = inp.shape[0]
        default_h = np.full(n, h0)
        e, n, u = lla2enu(inp[:, 0], inp[:, 1], inp[:, 2], lat0, lon0, h0)
        inp[:, 0], inp[:, 1], inp[:, 2] = e, n, u

        e, n, _ = lla2enu(inp[:, 3], inp[:, 4], default_h, lat0, lon0, h0)
        inp[:, 3], inp[:, 4] = e, n

        e, n, _ = lla2enu(inp[:, 5], inp[:, 6], default_h, lat0, lon0, h0)
        inp[:, 5], inp[:, 6] = e, n

        e, n, _ = lla2enu(inp[:, 7], inp[:, 8], default_h, lat0, lon0, h0)
        inp[:, 7], inp[:, 8] = e, n

    def __getitem__(self, item):
        item *= self._step
        pos = item + self._n_input
        inp = deepcopy(self._data[item:pos])
        lat0, lon0, _ = inp[-1, :3]
        self.convert_to_neu(inp, lat0, lon0, 0)

        outp = deepcopy(self._data[pos:(pos + self._n_output)])
        self.convert_to_neu(outp, lat0, lon0, 0)
        return \
            torch.from_numpy(self._transpose_func(inp)), \
            torch.from_numpy(self._transpose_func(outp)), \
            (lat0, lon0)  # origin

    def __len__(self):
        return self._size


@lru_cache(maxsize=8)
def _read_csv(filepath: str, **kwargs) -> pd.DataFrame:
    return pd.read_csv(filepath, **kwargs)


class TrajectoryDataset():
    def __init__(
            self, input_file, usecols=None,
            flight_id='flight_id', path_format="{date}_track_data_10.csv.gz"
    ):
        self.flight_data = list(pd.read_csv(input_file).itertuples(name='Row'))
        self._flight_id_dict = {r.flight_id: r.Index for r in self.flight_data}
        self._data_folder = Path(os.path.dirname(input_file))
        self._path_format = path_format
        self.usecols = usecols
        self._flight_id_str = flight_id

    def __getitem__(self, index):
        if isinstance(index, int):
            item = self.flight_data[index]
        elif isinstance(index, str):
            i = self._flight_id_dict[index]
            item = self.flight_data[i]
        else:
            raise TypeError()
        file_path = self._data_folder / self._path_format.format(date=item.date)
        df = _read_csv(file_path)
        df = df[df[self._flight_id_str] == item.flight_id]
        return df[self.usecols] if self.usecols else df

    def __len__(self):
        return len(self.flight_data)


class Status:
    train_running_loss = 0.0
    train_replacement_loss = 0.0
    train_velo_loss = 0.0
    train_iter = 121100

    valid_running_loss = 0.0
    valid_replacement_loss = 0.0
    valid_velo_loss = 0.0
    valid_iter = 34400


cos_similarity = nn.CosineSimilarity(dim=1, eps=1e-6)


def compute_loss(output, true_loc):
    mse = lambda v: (v**2).sum(axis=1).mean()
    mse2d = lambda v1, v2: mse(v1 - v2)
    pred_loc, velo = output
    replacement_loss = torch.sqrt(mse2d(pred_loc, true_loc))
    velo_loss = torch.sqrt(mse(velo[:, 1:, :] - velo[:, :-1, :]))
    cosine_loss = 1. - cos_similarity(velo[:, 1:, :],  velo[:, :-1, :]).mean()

    loss = replacement_loss  # + ALPHA * velo_loss + cosine_loss
    return loss, velo_loss, replacement_loss


def train(
        model: nn.Module,
        track: TrackDataset,
        optimizer: torch.optim.Optimizer,
        criterion,
        clip_value,
):
    model.train()
    track_loss = 0

    # usecols = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'phase', 'nwp_lat', 'nwp_lon']

    dataloader = DataLoader(track, batch_size=BATCH_SIZE,
                            shuffle=True, num_workers=8)

    n_pred = len(PREDICT_COLS)

    for i, (input_seq_np, output_seq_np, _) in enumerate(dataloader):
        input_seq = input_seq_np.to(device=device, dtype=dtype)
        # input_seq = [batch, n_points, n_features]
        output_seq = output_seq_np.to(device=device, dtype=dtype)
        # output_seq = [batch, n_points, n_features]

        inputs = input_seq.permute(0, 2, 1)  # [batch, n_features, n_points] for time-series
        outputs = output_seq[:, :, :n_pred]  # only get east, north, up

        waypoints = output_seq[:, :, n_pred:]

        optimizer.zero_grad()
        pred_outputs = model(inputs, outputs, waypoints, teacher_forcing_ratio=0.)
        loss, velo_loss, replacement_loss = compute_loss(pred_outputs, outputs)

        loss.backward()  # i = 819
        torch.nn.utils.clip_grad_norm_(model.parameters(), clip_value)
        optimizer.step()

        track_loss += loss.item()
        Status.train_running_loss += loss.item()
        Status.train_replacement_loss += replacement_loss.item()
        Status.train_velo_loss += velo_loss.item()
        Status.train_iter += 1
        if Status.train_iter % 100 == 0:    # every 100 mini-batches...

            # ...log the running loss
            writer.add_scalar('training replacement loss',
                            Status.train_replacement_loss / 100,
                            Status.train_iter)
            writer.add_scalar('training velocity loss',
                            Status.train_velo_loss / 100,
                            Status.train_iter)
            writer.add_scalar('training total loss',
                            Status.train_running_loss / 100,
                            Status.train_iter)

            # ...log a Matplotlib Figure showing the model's predictions on a
            # random mini-batch
            pred_output = pred_outputs[0][0, :, :2].cpu().detach().numpy()
            wp = np.unique(np.vstack((input_seq_np[0, :, 3:5], input_seq_np[0, :, 5:7])), axis=0)
            fig = plot_traj(input_seq_np[0, :, :2], output_seq_np[0, :, :2], pred_output, wp)
            fig.savefig(f'images/{version}/train_{Status.train_iter // 100}.jpg')
            writer.add_figure(
                'Trainning - predictions vs. actuals',
                fig,
                global_step=Status.train_iter)
            plt.close('all')
            Status.train_running_loss = 0.0
            Status.train_replacement_loss = 0.0
            Status.train_velo_loss = 0.0

    return track_loss / (i+1)


def evaluate(
        model: nn.Module,
        track: TrackDataset,
        criterion
):
    model.eval()
    track_loss = 0

    with torch.no_grad():
        dataloader = DataLoader(track, batch_size=BATCH_SIZE,
                                shuffle=True, num_workers=8, pin_memory=True)
        n_pred = len(PREDICT_COLS)

        for i, (input_seq_np, output_seq_np, _) in enumerate(dataloader):
            input_seq = input_seq_np.to(device=device, dtype=dtype)
            # input_seq = [batch, n_points, n_features]
            output_seq = output_seq_np.to(device=device, dtype=dtype)
            # output_seq = [batch, n_points, n_features]

            inputs = input_seq.permute(0, 2, 1)  # [batch, n_features, n_points] for time-series
            outputs = output_seq[:, :, :n_pred]  # only get lat, lon, alt

            waypoints = output_seq[:, :, n_pred:]

            n_points = outputs.shape[1]

            pred_outputs = model(inputs, n_points, waypoints, 0)
            loss, velo_loss, replacement_loss = compute_loss(pred_outputs, outputs)

            track_loss += loss.item()
            Status.valid_running_loss += loss.item()
            Status.valid_replacement_loss += replacement_loss.item()
            Status.valid_velo_loss += velo_loss.item()
            Status.valid_iter += 1
            if Status.valid_iter % 100 == 0:    # every 100 mini-batches...

                # ...log the running loss
                writer.add_scalar('Validation replacement loss',
                                Status.valid_replacement_loss / 100,
                                Status.valid_iter)
                writer.add_scalar('Validation velocity loss',
                                Status.valid_velo_loss / 100,
                                Status.valid_iter)
                writer.add_scalar('Validation total loss',
                                Status.valid_running_loss / 100,
                                Status.valid_iter)

                # ...log a Matplotlib Figure showing the model's predictions on a
                # random mini-batch
                pred_output = pred_outputs[0][0, :, :2].cpu().detach().numpy()
                wp = np.unique(np.vstack((input_seq_np[0, :, 3:5], input_seq_np[0, :, 5:7])), axis=0)
                fig = plot_traj(input_seq_np[0, :, :2], output_seq_np[0, :, :2], pred_output, wp)
                fig.savefig(f'images/{version}/eval_{Status.valid_iter // 100}.jpg')
                writer.add_figure(
                    'Validation - predictions vs. actuals',
                    fig,
                    global_step=Status.valid_iter)
                plt.close('all')
                Status.valid_running_loss = 0.0
                Status.valid_replacement_loss = 0.0
                Status.valid_velo_loss = 0.0

        return track_loss / (i+1)


enc = Encoder(INPUT_SEQ_LEN, LATENT_DIM, IN_CHANNELS, OUT_CHANNELS, KERNEL_SIZE)
dec = Decoder(DEC_INPUT_DIM, DEC_OUTPUT_DIM, LATENT_DIM)

model = TpDeepModel(enc, dec, device, dtype).to(device=device, dtype=dtype)
optimizer = optim.Adam(model.parameters(), lr=0.00005)
criterion = nn.MSELoss()
# criterion = nn.L1Loss()

# model.load_state_dict(torch.load(f'model/{version}/CONVO-GRU-model-server7.964.pt'))
# model.load_state_dict(torch.load(f'model/v6/CONVO-GRU-model-server2.838.pt'))


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs


def load_model(file_path: Path):
    import pickle
    copied_model = pickle.loads(pickle.dumps(model))
    copied_model.load_state_dict(torch.load(file_path))
    return copied_model


def main():

    best_valid_loss = float('inf')
    time_step = 30
    train_path = f'./data/train_data{time_step}/flight_data_small_train.csv'
    test_path = f'./data/train_data{time_step}/flight_data_small_test.csv'

    train_ds = TrajectoryDataset(train_path, usecols=USECOLS,
                                 path_format="{date}_track_data_" + str(time_step) + ".csv.gz")
    test_ds = TrajectoryDataset(test_path, usecols=USECOLS,
                                path_format="{date}_track_data_" + str(time_step) + ".csv.gz")

    max_points = 1000 * (30. / time_step)
    min_points = 150 * (30. /time_step)

    for epoch in range(N_EPOCHS):

        start_time = time.time()

        train_losses = []
        valid_losses = []

        for i in range(len(train_ds)):
            OUTPUT_SEQ_LEN = np.random.choice([15, 18, 21, 25])
            df = train_ds[i]
            # if train_ds.flight_data[i].flight_id != '1fcea567': continue
            if df.shape[0] < min_points or df.shape[0] > max_points: continue
            track = TrackDataset(df, n_input=INPUT_SEQ_LEN, n_output=OUTPUT_SEQ_LEN)

            train_loss_iter = train(model, track, optimizer, criterion, CLIP)
            train_losses.append(train_loss_iter)
            print(f'{train_ds.flight_data[i].flight_id} flight size: {df.shape}\t Train Loss: {train_loss_iter:.6f}, n_sequence: {OUTPUT_SEQ_LEN}')

        writer.flush()

        for i in range(len(test_ds)):
            OUTPUT_SEQ_LEN = np.random.choice([15, 18, 21, 25])
            df = test_ds[i]
            if df.shape[0] < min_points or df.shape[0] > max_points: continue
            track = TrackDataset(df, n_input=INPUT_SEQ_LEN, n_output=OUTPUT_SEQ_LEN)

            valid_loss_iter = evaluate(model, track, criterion)
            valid_losses.append(valid_loss_iter)
            print(f'{test_ds.flight_data[i].flight_id} flight size: {df.shape}\t Val. Loss: {valid_loss_iter:.6f}, n_sequence: {OUTPUT_SEQ_LEN}')

        end_time = time.time()
        writer.flush()

        epoch_mins, epoch_secs = epoch_time(start_time, end_time)
        train_loss = np.mean(train_losses)
        valid_loss = np.mean(valid_losses)

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            torch.save(model.state_dict(), f'model/{version}/CONVO-GRU-model-server{valid_loss:.3f}.pt')

        print(f'Epoch: {epoch + 1:02} | Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f}')
        print(f'\t Val. Loss: {valid_loss:.3f} - Best Loss: {best_valid_loss:.3f}')


if __name__ == '__main__':
    print(torch.__version__)
    print(device)
    main()

