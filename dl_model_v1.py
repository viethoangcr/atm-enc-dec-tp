from abc import ABC
import numpy as np
import pandas as pd
import random
import os
import time

from functools import lru_cache
from path import Path

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
import torch.nn.modules.loss as L
import torch.nn.functional as F



SEED = 7
random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(SEED)
torch.cuda.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
dtype = torch.float64
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
BATCH_SIZE = 160


class Encoder(nn.Module):
    def __init__(self, input_dim, output_dim, in_channels, out_channels, kernel_size, **kwargs):
        super().__init__(**kwargs)
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.input_dim = input_dim
        self.output_dim = output_dim

        self.kernel_size = kernel_size

        self.feature1 = nn.Sequential(
            nn.Conv1d(in_channels, out_channels, kernel_size=kernel_size),
            nn.ReLU(),
        )

        # self.feature12 = nn.Sequential(
        #     nn.Conv1d(in_channels, in_channels, kernel_size=kernel_size, groups=in_channels),
        #     nn.ReLU(),
        # )

        self.feature2 = nn.Sequential(
            nn.Linear(out_channels * (input_dim - kernel_size + 1),
                      output_dim),
            nn.ReLU(),
        )
        # self.feature2 = nn.Sequential(
        #     nn.Linear(out_channels * (input_dim - kernel_size + 1) + in_channels * (input_dim - kernel_size + 1),
        #               output_dim),
        #     nn.ReLU()
        # )
        # self.linear = nn.Linear(
        #     out_channels * (input_dim - kernel_size + 1), output_dim)

    def forward(self, input):
        # input = [batch, in_channels, input_dim]
        batch_size = input.shape[0]
        # x1 = x2 = [batch, output_dim]

        x = self.feature1(input)
        # x1 = self.feature1(input)
        # x2 = self.feature12(input)
        # x = torch.cat((x1, x2), dim=1)
        x = x.view(batch_size, -1)

        x = self.feature2(x)
        return x


class Decoder(nn.Module):
    def __init__(self, input_dim, output_dim, hid_dim, **kwargs):
        super().__init__(**kwargs)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hid_dim = hid_dim

        self.gru = nn.GRU(input_dim + hid_dim, hid_dim, batch_first=True)

        self.fc_out = nn.Linear(input_dim + hid_dim * 2, output_dim)

    def forward(self, input: torch.Tensor, context: torch.Tensor, hidden: torch.Tensor):
        # input = [batch, input_dim]
        # context = [batch, 1, hid_dim]
        # hidden = [1, batch, hid_dim]

        input1 = input.unsqueeze(1)
        x = torch.cat((input1, context), dim=2)
        # x = [batch, 1, input_dim + hid_dim]

        output, hidden = self.gru(x, hidden)
        # output = [batch, 1, hid_dim]
        # hidden = [1, batch, hid_dim]
        output_ = torch.cat((input, output.squeeze(1), hidden.squeeze(0)), dim=1)
        pred = self.fc_out(output_)
        # pred = [batch, output_dim]
        return pred, hidden


class TpDeepModel(nn.Module, ABC):
    def __init__(self, encoder, decoder, device, dtype, **kwargs):
        super().__init__(**kwargs)

        self.encoder = encoder
        self.decoder = decoder
        self.device = device
        self.dtype = dtype

        assert encoder.output_dim == decoder.hid_dim, \
            "Hidden dimensions of encoder and decoder must be equal!"

    def forward(self, inputs, outputs, x0, teacher_forcing_ratio=0.5):
        """

        Args:
            inputs: the input sequence
            outputs: training mode if outputs is a Tensor, predicting mode if outputs is
                an integer. In this case, the output sequence will have length of outputs
            teacher_forcing_ratio: the probability for training mode

        Returns:

        """

        # inputs = [batch, input_n_feature, seq_len]
        # outputs = [batch, output_seq_len, out_n_feature]
        # x0 = [batch, out_n_feature]
        # teacher_forcing_ratio
        assert isinstance(outputs, (torch.Tensor, int)), "Not a valid type for outputs"
        batch_size = inputs.shape[0]
        mode = 0 if isinstance(outputs, torch.Tensor) else 1  # 0: training, 1: predict
        output_seq_len = outputs.shape[1] if mode == 0 else outputs
        result = torch.zeros(batch_size, output_seq_len, self.decoder.output_dim).to(
            device=self.device, dtype=self.dtype)

        context = self.encoder(inputs)
        hidden = context.unsqueeze(0)
        context = context.unsqueeze(1)

        # input = torch.zeros(batch_size, self.decoder.output_dim).to(
        #     device=self.device, dtype=self.dtype)
        inp = x0

        for i in range(0, output_seq_len):
            output, hidden = self.decoder(inp, context, hidden)

            # outputs[:, i, :] = x0 + output
            result[:, i, :] = output

            teacher_force = random.random() < teacher_forcing_ratio
            if mode == 0 and teacher_force:
                inp = outputs[:, i, :]
            else:
                inp = output

        return result


def _compute_relative_location(x0: torch.Tensor, outputs: torch.Tensor):
    # x0 = [batch, 3]
    # outputs = [batch, seq_len, 3]

    res = torch.zeros_like(outputs)
    res[:, 1:, :] = outputs[:, 1:, :] - outputs[:, :-1, :]
    res[:, 0, :] = outputs[:, 0, :] - x0
    return res


def _compute_location(x0: torch.Tensor, rel_locs: torch.Tensor):
    # x0 = [batch, 3]
    # rel_locs = [batch, seq_len, 3]
    return rel_locs.cumsum(dim=1) + x0.unsqueeze(1)


class TrackDataset(Dataset):
    def __init__(
            self, track_data: pd.DataFrame, n_input, n_output, transpose=False,
            device=device, dtype=dtype
    ):
        self._data = track_data.values
        self._n_input = n_input
        self._n_output = n_output
        self._size = track_data.shape[0] - (n_input + n_output) + 1
        self._transpose_func = (lambda x: x.T) if transpose else (lambda x: x)
        self._dev = device
        self._dt = dtype
        assert self._size > 0

    def __getitem__(self, item):
        pos = item + self._n_input
        return \
            torch.from_numpy(self._transpose_func(self._data[item:pos])).to(device=self._dev, dtype=self._dt), \
            torch.from_numpy(self._transpose_func(self._data[pos:(pos + self._n_output)])).to(device=self._dev, dtype=self._dt)

    def __len__(self):
        return self._size


@lru_cache(maxsize=8)
def _read_csv(filepath: str, **kwargs) -> pd.DataFrame:
    return pd.read_csv(filepath, **kwargs)


class TrajectoryDataset():
    def __init__(
            self, input_file, usecols=None,
            flight_id='flight_id', path_format="{date}_track_data_10.csv.gz"
    ):
        self.flight_data = list(pd.read_csv(input_file).itertuples(name='Row'))
        self._flight_id_dict = {r.flight_id: r.Index for r in self.flight_data}
        self._data_folder = Path(os.path.dirname(input_file))
        self._path_format = path_format
        self.usecols = usecols
        self._flight_id_str = flight_id

    def __getitem__(self, index):
        if isinstance(index, int):
            item = self.flight_data[index]
        elif isinstance(index, str):
            i = self._flight_id_dict[index]
            item = self.flight_data[i]
        else:
            raise TypeError()
        file_path = self._data_folder / self._path_format.format(date=item.date)
        df = _read_csv(file_path)
        df = df[df[self._flight_id_str] == item.flight_id]
        return df[self.usecols] if self.usecols else df

    def __len__(self):
        return len(self.flight_data)


def train(
        model: nn.Module,
        track: TrackDataset,
        optimizer: torch.optim.Optimizer,
        criterion,
        clip_value,
):
    model.train()
    track_loss = 0

    # usecols = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'phase', 'nwp_lat', 'nwp_lon']

    dataloader = DataLoader(track, batch_size=BATCH_SIZE,
                            shuffle=True, num_workers=0)

    for i, (input_seq, output_seq) in enumerate(dataloader):
        input_seq = input_seq.to(device=device, dtype=dtype)
        # input_seq = [batch, n_points, n_features]
        output_seq = output_seq.to(device=device, dtype=dtype)
        # output_seq = [batch, n_points, n_features]

        inputs = input_seq.permute(0, 2, 1)  # [batch, n_features, n_points] for time-series
        outputs = output_seq[:, :, :3]  # only get lat, lon, alt
        outputs = _compute_relative_location(inputs[:, :3, -1], outputs)
        x0 = inputs[:, :3, -1] - inputs[:, :3, -2]  # only diff of get lat, lon, alt of the last 2 points

        optimizer.zero_grad()
        pred_loc = model(inputs, outputs, x0, teacher_forcing_ratio=0.20)
        loss = torch.sqrt(criterion(pred_loc.cumsum(dim=1), outputs.cumsum(dim=1)))

        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), clip_value)
        optimizer.step()

        track_loss += loss.item()

    return track_loss / (i+1)


def evaluate(
        model: nn.Module,
        track: TrackDataset,
        criterion
):
    model.eval()
    track_loss = 0

    with torch.no_grad():
        dataloader = DataLoader(track, batch_size=BATCH_SIZE,
                                shuffle=False, num_workers=0)

        for i, (input_seq, output_seq) in enumerate(dataloader):
            input_seq = input_seq.to(device=device, dtype=dtype)
            # input_seq = [batch, n_points, n_features]
            output_seq = output_seq.to(device=device, dtype=dtype)
            # output_seq = [batch, n_points, n_features]

            inputs = input_seq.permute(0, 2, 1)  # [batch, n_features, n_points] for time-series
            outputs = output_seq[:, :, :3]  # only get lat, lon, alt
            outputs = _compute_relative_location(inputs[:, :3, -1], outputs)
            x0 = inputs[:, :3, -1] - inputs[:, :3, -2]  # only diff of get lat, lon, alt of the last 2 points

            n_points = outputs.shape[1]

            pred_loc = model(inputs, n_points, x0, 0)
            loss = torch.sqrt(criterion(pred_loc.cumsum(dim=1), outputs.cumsum(dim=1)))

            track_loss += loss.item()

        return track_loss / (i+1)


def predict(model, input, n_points=30, batch=False):

    if not isinstance(input, torch.Tensor):
        dev = model.device
        dt = model.dtype
        input = torch.from_numpy(input).to(dev, dt)

    if not batch:
        input = input.unsqueeze(0)
    x0 = input[:, -1, :3]
    v0 = x0 - input[:, -2, :3]
    input = input.permute(0, 2, 1)
    res = model(input, n_points, v0, 0)
    res = _compute_location(x0, res)
    if not batch:
        return res.squeeze(0)
    else:
        res


N_EPOCHS = 100
CLIP = 1
USECOLS = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'phase', 'nwp_lat', 'nwp_lon']
INPUT_DIM = len(USECOLS)
IN_CHANNELS = len(USECOLS)
OUT_CHANNELS = 16
OUTPUT_DIM = 3
KERNEL_SIZE = 3
INPUT_SEQ_LEN = 10
OUTPUT_SEQ_LEN = 40  # 60
LATENT_DIM = 32
ALTITUDE_FACTOR = 1./35000

enc = Encoder(INPUT_SEQ_LEN, LATENT_DIM, IN_CHANNELS, OUT_CHANNELS, KERNEL_SIZE)
dec = Decoder(OUTPUT_DIM, OUTPUT_DIM, LATENT_DIM)

model = TpDeepModel(enc, dec, device, dtype).to(device=device, dtype=dtype)
optimizer = optim.Adam(model.parameters(), lr=0.0001)
criterion = nn.MSELoss()
# criterion = nn.L1Loss()

model.load_state_dict(torch.load(r'model/v1/CONVO-GRU-model-server0.142.pt'))


def epoch_time(start_time, end_time):
    elapsed_time = end_time - start_time
    elapsed_mins = int(elapsed_time / 60)
    elapsed_secs = int(elapsed_time - (elapsed_mins * 60))
    return elapsed_mins, elapsed_secs


def load_model(file_path: Path):
    import pickle
    copied_model = pickle.loads(pickle.dumps(model))
    copied_model.load_state_dict(torch.load(file_path))
    return copied_model


def normalized_alt(df: pd.DataFrame) -> pd.DataFrame:
    df['alt'] = df['alt'] * ALTITUDE_FACTOR
    return df


def main():

    best_valid_loss = float('inf')
    time_step = 15
    train_path = f'./data/train_data{time_step}/flight_data_small_train.csv'
    test_path = f'./data/train_data{time_step}/flight_data_small_test.csv'

    train_ds = TrajectoryDataset(train_path, usecols=USECOLS,
                                 path_format="{date}_track_data_" + str(time_step) + ".csv.gz")
    test_ds = TrajectoryDataset(test_path, usecols=USECOLS,
                                path_format="{date}_track_data_" + str(time_step) + ".csv.gz")

    max_points = 1000 * (30. / time_step)
    min_points = 200 * (30. /time_step)

    for epoch in range(N_EPOCHS):

        start_time = time.time()

        train_losses = []
        valid_losses = []

        for i in range(len(train_ds)):
            df = normalized_alt(train_ds[i])
            if df.shape[0] < min_points or df.shape[0] > max_points: continue
            track = TrackDataset(df, n_input=INPUT_SEQ_LEN, n_output=OUTPUT_SEQ_LEN)

            train_loss_iter = train(model, track, optimizer, criterion, CLIP)
            train_losses.append(train_loss_iter)
            print(f'{train_ds.flight_data[i].flight_id} flight size: {df.shape}\t Train Loss: {train_loss_iter:.6f}')

        for i in range(len(test_ds)):
            df = normalized_alt(test_ds[i])
            if df.shape[0] < 100 or df.shape[0] > max_points: continue
            track = TrackDataset(df, n_input=INPUT_SEQ_LEN, n_output=OUTPUT_SEQ_LEN)

            valid_loss_iter = evaluate(model, track, criterion)
            valid_losses.append(valid_loss_iter)
            print(f'{test_ds.flight_data[i].flight_id} flight size: {df.shape}\t Val. Loss: {valid_loss_iter:.6f}')

        end_time = time.time()

        epoch_mins, epoch_secs = epoch_time(start_time, end_time)
        train_loss = np.mean(train_losses)
        valid_loss = np.mean(valid_losses)

        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            torch.save(model.state_dict(), f'model/v1/CONVO-GRU-model-server{valid_loss:.3f}.pt')

        print(f'Epoch: {epoch + 1:02} | Time: {epoch_mins}m {epoch_secs}s')
        print(f'\tTrain Loss: {train_loss:.3f}')
        print(f'\t Val. Loss: {valid_loss:.3f} - Best Loss: {best_valid_loss:.3f}')


if __name__ == '__main__':
    print(torch.__version__)
    print(device)
    main()

    # from haversine import haversine, haversine_vector, Unit
    #
    # def distance(pred, true):
    #     pred = pred.cpu().numpy()
    #     true = true.cpu().numpy()
    #     return haversine_vector(pred[:, :2], true[:, :2], unit=Unit.METERS)
    #
    # model = load_model(r'model/CONVO-GRU-model-server.pt')
    # test_path = r'./data/train_data/flight_data_small_test.csv'
    # test_ds = TrajectoryDataset(test_path, usecols=USECOLS)
    # flight_id = '1fdfb403'
    # df = normalized_alt(test_ds[flight_id])
    # track = TrackDataset(df, n_input=INPUT_SEQ_LEN, n_output=OUTPUT_SEQ_LEN)
    #
    # model.eval()
    # track_loss = 0
    # res = []
    #
    # with torch.no_grad():
    #     dataloader = DataLoader(track, batch_size=1,
    #                             shuffle=False, num_workers=0)
    #
    #     for i, (input_seq, output_seq) in enumerate(dataloader):
    #         input_seq = input_seq.to(device=device, dtype=dtype)
    #         # input_seq = [batch, n_points, n_features]
    #         output_seq = output_seq.to(device=device, dtype=dtype)
    #         # output_seq = [batch, n_points, n_features]
    #
    #         inputs = input_seq.permute(0, 2, 1)  # [batch, n_features, n_points] for time-series
    #         outputs = output_seq[:, :, :3]  # only get lat, lon, alt
    #         x0 = inputs[:, -1, :3]  # only get lat, lon, alt of the last point
    #
    #         n_points = outputs.shape[1]
    #
    #         pred_loc = model(inputs, n_points, x0, 0)
    #         loss = criterion(pred_loc, outputs)
    #         print(loss.item())
    #
    #         res.append(distance(pred_loc[:, :, :2].squeeze(0), outputs[:, :, :2].squeeze(0)))
    #
    #         track_loss += loss.item()
    #
    # print(np.mean(res, axis=0))
