#

CLIP = 1
USECOLS = ['lat', 'lon', 'alt', 'wp_lat', 'wp_lon', 'phase', 'nwp_lat', 'nwp_lon']
INPUT_DIM = len(USECOLS)
IN_CHANNELS = len(USECOLS)
OUT_CHANNELS = 16
OUTPUT_DIM = 3
KERNEL_SIZE = 3
INPUT_SEQ_LEN = 10
OUTOUT_SEQ_LEN = 60
LATENT_DIM = 128
ALTITUDE_FACTOR = 1./35000

enc = Encoder(INPUT_SEQ_LEN, LATENT_DIM, IN_CHANNELS, OUT_CHANNELS, KERNEL_SIZE)
dec = Decoder(OUTPUT_DIM, OUTPUT_DIM, LATENT_DIM)

model = TpDeepModel(enc, dec, device, dtype).to(device=device, dtype=dtype)