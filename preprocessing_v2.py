from enum import unique
import os
import logging
import json
import math
from itertools import groupby

import pandas as pd
import numpy as np
from pandas.io.parsers import read_csv
from scipy.interpolate import CubicSpline, interp1d
from haversine import haversine_vector, Unit, haversine
import matplotlib.pyplot as plt

from config import output_path, dataset_folder
from dataset import get_flight_data
from utils import MultiInterpolation, curvature, mapping_waypoints, LatLonInterpolation, curvature_latlon, geod

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

TIME_STEP = 15
COMMENT = '_VIR_WP'
# COMMENT = ''
MANEUVERS_PERIOD = math.ceil(20 * 60 / TIME_STEP) + 1  # check every 20 minutes


def filter_data():
    files = os.listdir(output_path)
    waypoints = [pd.read_csv(output_path / file) for file in files if file.endswith('_waypoints.csv')]
    waypoint_df = pd.concat(waypoints, ignore_index=True)
    waypoint_df = waypoint_df[['waypointname', 'latitude', 'longitude']].drop_duplicates('waypointname')
    waypoint_df = waypoint_df[~waypoint_df['waypointname'].isin(('T_O_C', 'T_O_D'))]
    waypoint_df.rename({
        'waypointname': 'IDENT', 'latitude': 'LATITUDE', 'longitude': 'LONGITUDE'
    }, axis=1, inplace=True)

    waypoint_sg = pd.read_csv('./additional_data/singapore_waypoint_adsb.csv', usecols=('name', 'lat', 'lon'))
    waypoint_sg.rename({
        'name': 'IDENT', 'lat': 'LATITUDE', 'lon': 'LONGITUDE'
    }, axis=1, inplace=True)

    flight_df = pd.read_csv(dataset_folder / 'ADSB_2019_6MONTH_FLIGHTS.csv.gz')
    flight_df = flight_df.drop_duplicates('flight_id', keep=False)
    flight_df = flight_df[(flight_df['flighttime_average'] > 3600 * 2) & (flight_df['flighttime_average'] < 3600 * 7) &
                          (flight_df['date'] <= '2019-06-30')]

    flight_idx = flight_df["flight_id"].values
    flight_data, track_data = get_flight_data(None, flight_filter={
        'flight_id': lambda x: x.isin(flight_idx),
    })

    track_data = track_data[
        (0 <= track_data.lat) &
        (track_data.lat <= 25) &
        (85 <= track_data.lon) &
        (track_data.lon <= 125)
    ]

    from aviation_waypoints import get_waypoints
    wps = get_waypoints(update=False)
    wps = pd.concat([wps, waypoint_df, waypoint_sg], ignore_index=True)
    all_wps = wps[(0 <= wps.LATITUDE) & (wps.LATITUDE <= 25) & (85 <= wps.LONGITUDE) & (wps.LONGITUDE <= 125)]
    all_wps = all_wps.drop_duplicates('IDENT', keep='last').\
        drop_duplicates(['LATITUDE', "LONGITUDE"], keep='first')

    track_data = track_data[track_data['calibrated_altitude_ft'] > 10]
    gps = track_data.groupby('flight_id')
    timed = gps['UTC'].max() - gps['UTC'].min()
    tdiff = gps['UTC'].apply(lambda x: x.diff().max())

    idx = set(timed[timed < 3600 * 7.5].index.values).intersection(tdiff[tdiff < 1000].index.values)

    # interval = 15 * 60
    # wps_dict = {}
    # for fid, track_df in gps:
    #     if fid not in idx: continue
    #     wps = []
    #
    #     break
    #
    #
    # gps = waypoint_df[['waypointname', 'latitude', 'longitude']].groupby(
    #     waypoint_df['flight_id'], group_keys=False
    # )
    #
    # wps_dict = {
    #     id_mapping[fid]: df.to_dict('records') for fid, df in gps if fid in id_mapping
    # }
    #
    # # export to json
    # with open('data/flight_plan.json', 'wt') as fp:
    #     json.dump(wps_dict, fp)


    flight_data[flight_data['flight_id'].isin(idx)].to_csv('data/flight_data_v2.csv.gz', index=False)
    track_data[track_data['flight_id'].isin(idx)].to_csv('data/track_data_v2.csv.gz', index=False)
    all_wps.to_csv('data/all_waypoints_v2.csv.gz', index=False)


all_wps = pd.read_csv('data/all_waypoints_v2.csv.gz')
def interpolate_track(args):
    fid, df = args
    df = df[df['calibrated_altitude_ft'] > 29000]
    df = df.sort_values('UTC')
    df = df.drop_duplicates(['lat', 'lon'], keep='last')

    t = df[['lat', 'lon']]
    dis = haversine_vector(t.values[:-1, :], t.values[1:, :], unit=Unit.METERS)
    time_diff = df['UTC'].diff().values[1:]
    speed = dis / time_diff
    if np.any(speed > 600) or np.any(speed < 10):
        logger.info(f"Ignore flight {fid} with min speed {speed.min()} and max_speed {speed.max()}")
        return fid, None

    if (df['date'].unique().size > 1) or (df.shape[0] < 100) or df['date'].iloc[0] > '2019-04-30':
        return fid, None
    # new_df = df.iloc[1:, :]
    # new_df['speed'] = speed
    # return fid, new_df

    utc = df['UTC'].values
    lat, lon, alt, ground_speed, heading = df['lat'].values, df['lon'].values, df['calibrated_altitude_ft'].values, df['ground_speed_kts'].values, df['heading'].values

    # pre-compute heading
    heading[:-1], _, _ = geod.inv(lon[:-1], lat[:-1], lon[1:], lat[1:])

    f = MultiInterpolation(utc, lat, lon, alt, ground_speed, inter_func=CubicSpline)
    nts = np.arange((utc[0] // TIME_STEP + 1) * TIME_STEP, utc[-1], TIME_STEP)
    nlat, nlon, nalt, gspeed = f(nts)

    fheading = interp1d(utc, heading, kind='previous')
    nheading = fheading(nts)

    t = np.array([nlat, nlon]).T
    dis = haversine_vector(t[:-1, :], t[1:, :], unit=Unit.NAUTICAL_MILES)
    computed_speed = dis / (TIME_STEP/3600.)
    gspeed[1:] = computed_speed

    curvature_ = curvature_latlon(np.array((nlat, nlon, nalt)).T, step=10)
    new_df = pd.DataFrame({
        'flight_id': fid,
        'timestamp': nts,
        'lat': nlat,
        'lon': nlon,
        'alt': nalt,
        'ground_speed': gspeed,
        'heading': nheading,
        'curvature': curvature_,
    })

    # max_points = 1000 * (30. / TIME_STEP)
    # min_points = 150 * (30. / TIME_STEP)
    # if new_df.shape[0] < min_points or new_df.shape[0] > max_points:
    #     return fid, None
    # else:
    #     return fid, new_df

    wps = mapping_waypoints(
        new_df, all_wps, max_distance=7, max_distance_straight=2., min_waypoint_distance=20,
        curvature_threshold=1./80, traj_column_name=('lon', 'lat', 'timestamp'), create_virtual_waypoint=True)
    if len(wps) < 2: return fid, None
    wps_df = pd.DataFrame(wps)


    if nts.max() - wps_df['posix_time'].iloc[-1] > 60:  # add last point to wp
        wps_df = wps_df.append({
            'distance': 0, 'p_lat': nlat[-1], 'p_lon': nlon[-1],
            'wp_lat': nlat[-1], 'wp_lon': nlon[-1], 'posix_time': nts[-1],
            'ident': 'END_POINT', 'tr_idx': len(nlat)-1,
        }, ignore_index=True)

    wps_df['wp_speed'] = new_df.loc[wps_df.tr_idx.values, "ground_speed"].values
    wps_df['wp_alt'] = new_df.loc[wps_df.tr_idx.values, "alt"].values

    # new_ts = []
    # ts = wps_df['posix_time'].values
    # latlon_interp = LatLonInterpolation(ts, wps_df[['wp_lat', 'wp_lon']].values)
    # max_range = 8. * 60
    # for i in range(1, len(ts)):
    #     if ts[i] - ts[i-1] > max_range:
    #         n_step = math.ceil((ts[i] - ts[i-1])/max_range)
    #         step = (ts[i] - ts[i-1] + .1) / n_step  # +.1 is to avoid float number computation error
    #         new_ts += list(np.arange(ts[i-1]+step, ts[i], step))
    # if len(new_ts) > 0:
    #     wp_locs = np.array(latlon_interp(new_ts))
    #     dummy_wp_df = pd.DataFrame({
    #         'ident': [f'DUMMY_{i}' for i in range(len(new_ts))],
    #         'posix_time': new_ts,
    #         'wp_lat': wp_locs[:, 0], 'wp_lon': wp_locs[:, 1]
    #     })
    #     wps_df = pd.concat((wps_df, dummy_wp_df), ignore_index=True).sort_values('posix_time', ignore_index=True)

    # find the closest value of timestamp to posix_time
    pos = 0
    res = []
    pt = wps_df['posix_time'].values
    for i, v in enumerate(nts):
        if v >= pt[pos]:
            res.append(i - 1)
            pos += 1
            if pos == len(pt): break
    wp_df = wps_df[['ident', 'wp_lat', 'wp_lon', 'wp_speed', 'wp_alt']]
    try:
        wp_df = wp_df.assign(index=res)
    except Exception as e:
        logger.exception(e)
        return fid, None
    wp_df = wp_df.set_index('index')
    next_wp_df = wp_df.shift(-1)
    prev_wp_df = wp_df.shift(1)
    prev_wp_df.iat[0, 0] = 'START_POINT'
    prev_wp_df.iat[0, 1] = new_df['lat'].iloc[0]
    prev_wp_df.iat[0, 2] = new_df['lon'].iloc[0]

    next_wp_df.rename({
        'ident': 'nwp_name', 'wp_lat': 'nwp_lat', 'wp_lon': 'nwp_lon', 'wp_speed': 'nwp_speed', 'wp_alt': 'nwp_alt'
    }, axis=1, inplace=True)
    prev_wp_df.rename({
        'ident': 'pwp_name', 'wp_lat': 'pwp_lat', 'wp_lon': 'pwp_lon', 'wp_speed': 'pwp_speed', 'wp_alt': 'pwp_alt'
    }, axis=1, inplace=True)
    wp_df = wp_df.join(next_wp_df, how='left').join(prev_wp_df, how='left')

    new_df = new_df.join(wp_df, how='left')
    new_df = new_df.bfill().ffill()

    return fid, new_df


def re_sampling_data(time_step=TIME_STEP):
    # with open('data/flight_plan.json', 'rt') as fp:
    #     wps_dict = json.load(fp)

    track_data = pd.read_csv('data/track_data_v2.csv.gz')
    flight_data = pd.read_csv('data/flight_data_v2.csv.gz')

    gps = track_data.groupby('flight_id')
    res_dict = {}
    fids = []

    from multiprocessing import Pool
    with Pool(6) as p:
        res = p.map(interpolate_track, gps)

    # res = [interpolate_track(g) for g in gps]

    ignore_counts = 0
    for fid, new_df in res:
        if new_df is None:
            ignore_counts +=1
            continue
        res_dict[fid] = new_df
        fids.append(fid)

    logger.info(f"Finished processing {len(res_dict)} flights, ignore {ignore_counts} flights                                                                                                                                                                                                     ")

    flight_data = flight_data[flight_data['flight_id'].isin(fids)]
    flight_data.to_csv(f'data/train_data{TIME_STEP}{COMMENT}/flight_data.csv', index=False)

    fgps = flight_data.groupby('date')

    for date, df in fgps:
        logger.info(f"Saving data for date {date}")
        res = [res_dict[fid] for fid in df['flight_id'].values]
        logger.info(f"Date {date} has {len(res)} flights")
        new_track_data = pd.concat(res, ignore_index=True)
        new_track_data.to_csv(
            f'data/train_data{TIME_STEP}{COMMENT}/{date}_track_data_{time_step}.csv.gz', index=False, float_format="%.9f"
        )


def split_train_test():
    df = pd.read_csv(f'data/train_data{TIME_STEP}{COMMENT}/flight_data.csv')
    train_df = df[df['date'] <= '2019-03-25']
    test_df = df[(df['date'] > '2019-03-25') & (df['date'] <= '2019-04-30')]
    # train_df = df[df['date'] <= '2019-03-31']
    # test_df = df[(df['date'] > '2019-03-31') & (df['date'] <= '2019-04-07')]
    train_df.to_csv(f'data/train_data{TIME_STEP}{COMMENT}/flight_data_small_train.csv', index=False)
    test_df.to_csv(f'data/train_data{TIME_STEP}{COMMENT}/flight_data_small_test.csv', index=False)


def generate_bluesky_scene(output_file, track: pd.DataFrame, ac_type_map: dict, duration=40*15, dist_threshold=5):
    track['sim_ts'] = (track['timestamp'] - track['timestamp'].min())
    result = []
    for row in track.itertuples():
        ac_id = f'{row.flight_id}_{row.Index}'
        cmdstr = f'CRE {ac_id}, {ac_type_map[row.flight_id]}, {row.lat:.7f}, {row.lon:.7f}, ' \
                 f'{row.heading:.2f}, {row.alt:.4f}, {row.ground_speed:.4f}'
        result.append((row.sim_ts, cmdstr))
        if haversine((row.lat, row.lon), (row.wp_lat, row.wp_lon), unit=Unit.NAUTICAL_MILES) > 10:
            cmdstr = f'ADDWPT {ac_id}, {row.wp_lat},{row.wp_lon},{row.wp_alt},{row.wp_speed}'
            result.append((row.sim_ts+1, cmdstr))
        cmdstr = f'ADDWPT {ac_id}, {row.nwp_lat},{row.nwp_lon},{row.nwp_alt},{row.nwp_speed}'
        result.append((row.sim_ts+2, cmdstr))
        cmdstr = f'DEL {ac_id}'
        result.append((row.sim_ts+duration+1, cmdstr))
    result.sort()

    with open(output_file, 'wt') as fp:
        for row in result:
            time_str = f"{int(row[0] // 3600)}:{int((row[0] // 60) % 60)}:{row[0] % 60:.2f}"
            fp.write(f"{time_str}>{row[1]}\n")


def export_bluesky_scene(): 
    flights = pd.read_csv(f'data/train_data{TIME_STEP}{COMMENT}/flight_data_small_test.csv')
    dates = flights['date'].unique()
    ac_type_map = {v[0]: v[1] for v in flights[['flight_id', 'code']].values}

    for date in dates:
        track = pd.read_csv(f'data/train_data{TIME_STEP}{COMMENT}/{date}_track_data_{TIME_STEP}.csv.gz')
        logger.info(f'Generating scenario file for date {date}')
        generate_bluesky_scene(f'data/train_data{TIME_STEP}{COMMENT}/scn_{date}_track_data_{TIME_STEP}.scn', track, ac_type_map=ac_type_map)


if __name__ == "__main__":
    from path import Path
    # filter_data()
    #
    path = Path(f'data/train_data{TIME_STEP}{COMMENT}')
    if not path.exists():
        path.mkdir()
    re_sampling_data()
    # split_train_test()
    export_bluesky_scene()
